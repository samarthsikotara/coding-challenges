## Coding challenge Risk Alerts
Create a highly interactive map, perform spatial analyses to deliver critical security information in one place

- Real time crime statistics across all cities in India
- Identity high/medium/low risk zones in India
- Real time travel updates. It allows travelers and organizations to get vetted news alerts and security reports by city/country
- Bonus point if you use predictive analytics.

### Instructions
* Maintain your code on github along with proper readme files containing all instructions to setup and run the code
* Follow standard coding practices of the language.
